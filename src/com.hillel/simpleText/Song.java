package com.hillel.simpleText;

import java.lang.reflect.Array;
import java.util.*;

public class Song {

    public static void main(String[] args) {
        String s = "This ain't a song for the broken-hearted. " +
                "No silent prayer for the faith-departed. " +
                "I ain't gonna be just a face in the crowd, " +
                "You're gonna hear my voice, when I shout it out loud. " +
                "It's my life, It's now or never, I ain't gonna live forever. " +
                "I just wanna live while I'm alive. It's my life. My heart is  like  an open highway," +
                "Like Frankie said I did it my way, I just wanna live while I'm alive, It's my life." +
                "This is for the ones who stood their ground. For Tommy and Gina who never backed down." +
                "Tomorrow's getting harder make no mistake,Luck ain't even lucky. Got to make your own breaks. ";
        finder(s);

    }

    private static Array[] finder(String s) {

        String[] words = s.toLowerCase().replaceAll("[-.?!)(,':]", "").split("\\s");
        if (words.length < 3) {
            return new Array[0];
        }
        Map<String, Integer> counterMap = new HashMap<>();
        for (String word : words) {
            if (!word.isEmpty()) {
                Integer count = counterMap.get(word);
                if (count == null) {
                    count = 0;
                }
                counterMap.put(word, ++count);
            }
        }

        counterMap.entrySet().stream()
                .sorted((k1, k2) -> -k1.getValue().compareTo(k2.getValue()))
                .limit(3)
                .forEach(k -> System.out.println(k.getKey() + ": " + k.getValue()));

        return new Array[0];

    }
}






