package com.hillel.simpleCipher;


public class Separator {

    public String doSep(String s, int n) {

        if (s == null || s.isEmpty()) {
            return s;
        } else if (n <= 0) {
            return s;
        } else {
            for (int i = 0; i < n; i++) {

                char[] array = s.toCharArray();
                int count = 0;
                StringBuilder even = new StringBuilder();
                StringBuilder odd = new StringBuilder();
                for (char ch : array) {
                    if (count % 2 == 0) {
                        even.append("" + ch);
                    } else {
                        odd.append("" + ch);
                    }
                    count++;
                }
                s = odd.toString() + even.toString();
            }
        }

        return s;
    }
}

