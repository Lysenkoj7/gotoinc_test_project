package com.hillel.simpleCipher;

import javax.crypto.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class MyCipher {

    public static void main(String[] args) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        Separator Separator = new Separator();
        String s = Separator.doSep("TopSecret", 3);
        System.out.println(s);
        Cipher cipher = Cipher.getInstance("AES");
        System.out.print("\n");


        KeyGenerator gen = KeyGenerator.getInstance("AES");
        gen.init(128);
        SecretKey key = gen.generateKey();

        if (s == null || s.isEmpty()) {
            System.out.println("Входящая строка пустая или NULL");
        } else {
            byte[] bytes = encrypt(s, cipher, key);

            decrypt(key, bytes);
        }
    }

    private static byte[] encrypt(String s, Cipher cipher, SecretKey key) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        cipher.init(Cipher.ENCRYPT_MODE, key);

        byte[] bytes = cipher.doFinal(s.getBytes());
        for (byte b : bytes) {
            System.out.print(b);
        }
        System.out.println("\n");
        return bytes;

    }

    private static void decrypt(SecretKey key, byte[] bytes) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        Cipher decryptCipher = Cipher.getInstance("AES");

        decryptCipher.init(Cipher.DECRYPT_MODE, key);
        byte[] decryptedBytes = decryptCipher.doFinal(bytes);
        for (byte b : decryptedBytes) {
            System.out.print((char) b);

        }
    }
}
